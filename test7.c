#include<stdio.h>
#include<stdlib.h>
void input(int a[10][10],int m,int n)
{
	for (int i=1; i<=m; i++)
	{
		for (int j=1; j<=n; j++)
		{
			printf("Enter matrix element [%d,%d]: ",i,j);
			scanf("%d",&a[i][j]);
		}
	}
}
void transpose(int a[10][10],int t[10][10],int m,int n)
{      
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=n;j++)
			t[j][i]=a[i][j];
		printf("\n");
	}
}
void display(int t[10][10],int m,int n)
{
	for (int i=1;i<=m; i++)
	{
		for (int j=1;j<=n; j++)
		{
			printf("\t%d ",t[i][j]);
		}
		printf("\n");
	}
}
int main()
{
	system("clear");
	int m,n,a[10][10],t[10][10];
	printf("enter the number of rows=");
	scanf("%d",&m);
	printf("enter the number of columns=");
	scanf("%d",&n);
	input(a,m,n); //takes the input to the array
	printf("array elements are\n");
	display(a,m,n); //displays the original array elements
	transpose(a,t,m,n); //transpose of matrix
	printf("array elements after transpose are\n");
	display(t,n,m); //displays the transpose matrix
	return 0;
}
