#include<stdio.h>
#include<stdlib.h>
#include<math.h>
int main()
{
    system("clear");
    int a,b,c,s;
    float d,x1,x2;
    printf("enter the coefficients of the quadratic equation\n");
    scanf("%d%d%d",&a,&b,&c);
    d=pow(b,2)-(4*a*c);
    if(d>0)
        s=1;
    else if(d==0)
        s=2;
    else 
        s=3;
    switch(s)
    {
        case 1:printf("roots are real and unequal\n");
            x1=(-b+sqrt(d))/(2*a);
            x2=(-b-sqrt(d))/(2*a);
            printf("the roots are x1=%f and x2=%f\n",x1,x2);
            break;
        case 2:printf("roots are real and equal\n");
            x1=-b/(2*a);
            x2=x1;
            printf("the roots are x=%f\n",x1);
            break;	 
        default:printf("roots are imaginary\n");
    }
    return 0;
}