#include<stdio.h>
struct name
{
    char firstname[30];
    char lastname[30];
};
struct student
{
    int rollno;
    struct name n;
    char dept[3];
    char section[2];
    float fees;
    int TotalMarks;
};
int main()
{
    struct student s[2];

    for(int i=0;i<2;i++)
    {
        printf("\nEnter the details of student%d:\n",i+1);
        printf("enter the roll number\n");
        scanf("%d",&s[i].rollno);
        printf("enter the first name\n");
        scanf("%s",s[i].n.firstname);
        printf("enter the last name\n");
        scanf("%s",s[i].n.lastname);
        printf("Enter the department\n");
        scanf("%s",s[i].dept);
        printf("Enter the section\n");
        scanf("%s",s[i].section);
        printf("enter the fee\n");
        scanf("%f",&s[i].fees);
        printf("enter the total marks obtained by the student out of 500\n");
        scanf("%d",&s[i].TotalMarks);
    }
    int m=(s[0].TotalMarks>s[1].TotalMarks)?0:1;
    printf("\n******************DETAILS OF STUDENT WHO SCORED HIGHEST******************\n");
    printf("ROLL NUMBER\t=%d\n",s[m].rollno);
    printf("FIRST NAME\t=%s\n",s[m].n.firstname);
    printf("LAST NAME\t=%s\n",s[m].n.lastname);
    printf("SECTION   \t=%s\n",s[m].section);
    printf("DEPARTMENT\t=%s\n",s[m].dept);
    printf("FEE PAID\t=%f\n",s[m].fees);
    printf("TOTAL MARKS\t=%d\n",s[m].TotalMarks);
    return 0;
}

