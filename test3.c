#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define pi 3.14
float input()
{
	float r;
	printf("enter the radius of circle\n");
	scanf("%f",&r);
	return r;
}
float compute(float r)
{
	float area=pi*pow(r,2);
	return area;
}
void disp(float a)
{
	printf("area=%f",a);
}
int main()
{
	system("clear");
	float r,area;
	r=input();
	area=compute(r);
	disp(area);
	return 0;
}