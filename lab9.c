#include<stdio.h>
void swap(int *p1,int *p2)
{
    int temp=*p1;
    *p1=*p2;
    *p2=temp;
}

int main()
{
    int a,b;
    printf("enter the first number=");
    scanf("%d",&a);
    printf("Enter the second number=");
    scanf("%d",&b);
    swap(&a,&b);
    printf("\nFirst number=%d",a);
    printf("\nsecond number=%d",b);
    return 0;
}