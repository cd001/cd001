#include<stdio.h>
#include<stdlib.h>
int main()
{
	system("clear");
	int m,n;
	printf("Enter the number of rows=");
	scanf("%d",&m);
	printf("Enter the number of columns=");
	scanf("%d",&n);
	int a[m][n],b[m][n],sum[m][n],difference[m][n];
	printf("enter the elements of first array=\n");
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=n;j++)
		{
			scanf("%d",&a[i][j]);
		}
	}
	printf("enter the elements of second array=\n");
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=n;j++)
		{
			scanf("%d",&b[i][j]);
		}
	}
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=n;j++)
		{
			sum[i][j]=a[i][j]+b[i][j];
			difference[i][j]=a[i][j]-b[i][j];
		}
	}
	printf("The sum of the two 2-dimentional array elements are:\n");
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=n;j++)
		{
			printf("%d\t",sum[i][j]);
		}
		printf("\n");
	}
	printf("The difference of the two 2-dimentional array elements are:\n");
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=n;j++)
		{
			 printf("%d\t",difference[i][j]);
		}
		printf("\n");
	}
	return 0;
}