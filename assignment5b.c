#include<stdio.h>
#include<stdlib.h>
int main()
{	
	system("clear");
	int n;
	int odd[]={1,3,5,7,9,11,13,15};

	for(int i=0;i<10;i++)
	{
		if(odd[i]!=0)
			n++;
	}

	printf("array elements before deletion of 5\n");
	for(int i=0;i<n;i++)
		printf("%d ",odd[i]);
	printf("\n");

	for(int i=3;i<n;i++)
		odd[i-1]=odd[i];

	printf("after deletion\n");
	for(int i=0;i<n-1;i++)
		printf("%d ",odd[i]);

	return 0;
}
