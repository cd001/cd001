#include<stdio.h>
//funtion to add 2 nos
void add(int *pn1,int *pn2,int *psum)
{
    *psum=*pn1+*pn2;
    printf("the sum of 2 numbers=%d\n",*psum);
}
//funtion to subtract 2 nos
void subtract(int *pn1,int *pn2,int *pdiff)
{
    *pdiff=*pn1-*pn2;
    printf("the difference of 2 numbers=%d\n",*pdiff);
}
//funtion to find product 2 nos
void multiply(int *pn1,int *pn2,int *pprod)
{
    *pprod=(*pn1)*(*pn2);
    printf("the product of 2 numbers=%d\n",*pprod);
}
//funtion to find quotient 2 nos
void divide(int *pn1,int *pn2,int *pquot)
{
    *pquot=(*pn1)/(*pn2);
    printf("the quotient of 2 numbers=%d\n",*pquot);
}
int main()
{
    int n1,n2;
    int sum,difference,product,quotient;
    printf("enter the 2 numbers=\n");
    scanf("%d%d",&n1,&n2);
    add(&n1,&n2,&sum);
    subtract(&n1,&n2,&difference);
    multiply(&n1,&n2,&product);
    divide(&n1,&n2,&quotient);
    return 0;
}