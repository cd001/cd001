#include<stdio.h>
#include<stdlib.h>
#include<math.h>
float input()
{
    float a;
	scanf("%f",&a);
	return a;
}
float calculate(float a, float b,float c)
{
	float s,area;
	s=(a+b+c)/2.0;
	area=sqrt(s*(s-a)*(s-b)*(s-c));
	return area;
}
void display(float area)
{
	printf("area of the triangle=%f",area);
}
int main()
{
	system("clear");
	float a,b,c,area;
	printf("enter the 3 sides of the triangle=\n");
	a=input();
	b=input();
	c=input();
	area=calculate(a,b,c);
	display(area);
	return 0;
}