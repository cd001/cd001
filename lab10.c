#include<stdio.h>
int main()
{
	FILE *fp;
	char c;
	
	printf("Input the data:\n");
	fp=fopen("INPUT.dat","w"); /*opens the file INPUT*/
	while((c=getchar())!=EOF)  /*gets a character from keyboard*/
		putc(c,fp);            /*writes the character to file INPUT*/
	fclose(fp);                /*closes the file INPUT*/
	
	printf("\nOutput data:\n");
	fp=fopen("INPUT.dat","r"); /*reopens the file INPUT*/
	while((c=getc(fp))!=EOF)   /*reads a character from INPUT*/
		printf("%c",c);        /*prints a character from INPUT*/
	fclose(fp);                /*closes the file INPUT*/
	
	return 0;
}