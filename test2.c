#include<stdio.h>
#include<stdlib.h>
#include<math.h>
int main()
{
	system("clear");
	float a,b,c;
	float x1,x2,d,imaginary;
	printf("enter the coefficients\n");
	scanf("%f%f%f",&a,&b,&c);
	d=pow(b,2)-4*a*c;

	switch(d>0)
	{
		case 1:x1=(-b+sqrt(d))/(2*a);
			x2=(-b-sqrt(d))/(2*a);
			printf("the roots are real and unequal\n");
			printf("root 1=%f root 2=%f",x1,x2);
			break;
		case 0:
			switch(d<0)
			{
				case 0:x1=x2=-b/(2*a);
					printf("roots are real and equal\n");
					printf("roots 1=%f and roots 2=%f",x1,x2);
					break;
				case 1: x1=x2=-b /(2*a);
					imaginary=sqrt(-d)/(2*a);
					printf("the roots are imaginary \n");
					printf("root1 = %f+i%f and root2 = %f-i%f", x1, imaginary,x2, imaginary);
					break;
			}
	}
	return 0;
}
